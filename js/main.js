const countdown = document.querySelector('.countdown')

// setting the launch date

const launchDate = new Date('Nov 21 2018 23:59:59').getTime();

// update every seconds

const intvl = setInterval(()=>{

  // Get todays Date and time (ms)

  const now = new Date().getTime();

  // get distance from now to launch day

  const distance = launchDate -now;

  // Time calculations
  const days = Math.floor(distance/(1000*60*60*24));

  const hours = Math.floor((distance%(1000*60*60*24))/(1000*60*60));


  

  const mins = Math.floor((distance%(1000*60*60))/(1000*60));

  const seconds = Math.floor((distance%(1000*60))/(1000));

  // Display the Results

  countdown.innerHTML = `
    <div>${days}<span>Days</span></div>
    <div>${hours}<span>Hours</span></div>
    <div>${mins}<span>Minutes</span></div>
    <div>${seconds}<span>Seconds</span></div>
  `;

  // If launch date passed
  if (distance<0) {
    //sto countdown
    clearInterval(intvl);

    // style and output text

    countdown.style.color = '#17a2b8';
    countdown.innerHTML = 'Launched!';
  }

}, 1000 );
